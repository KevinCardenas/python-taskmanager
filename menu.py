import os, sys, json
from inicarsesion import iniciar_sesion
from bookmarks import create_bookmark, erase_bookmark, modify_bookmark, view_allbookmarks, view_bookmark, view_label_bookmarks
from labels import create_label, delete_label, modify_label
from crearusuariosjson import create_user

os.system('cls')

print(f'MENU PRINCIPAL')
print(F'Digita la opcion que deseas usar')
print(F'1. Iniciar Sesion')
print(F'2. Registrarse')
print(F'3. Salir')
x=int(input())
if x==1:
    logged=iniciar_sesion()
    while True:
        if logged == False:
            sys.exit()
        else:
            user=logged['name']
            print(f'\n\nMENU PRINCIPAL - {user}')
            print(F'Digita la opcion que deseas usar')
            print(F'1. Cerrar Sesion')
            print(F'2. Agregar marcador')
            print(F'3. Modificar marcador')
            print(F'4. Eliminar marcador')
            print(F'5. Agregar Label')
            print(F'6. Modificar Label')
            print(F'7. Eliminar Label')
            print(F'8. Ver todos los marcadores')
            print(F'9. Ver marcadores de un label')
            print(F'10. Ver marcador especifico')
            y=int(input())
            if y==1:
                logged==False
                sys.exit()
            elif y==2:
                create_bookmark(logged)
            elif y==3:
                modify_bookmark(logged)
            elif y==4:
                erase_bookmark(logged)
            elif y==5:
                create_label(logged)
            elif y==6:
                modify_label(logged)
            elif y==7:
                delete_label(logged)
            elif y==8:
                view_allbookmarks(logged)
            elif y==9:
                view_label_bookmarks(logged)
            elif y==10:
                view_allbookmarks(logged)
                z = int(input('Digita el marcador del que deseas ver mas informacion: '))
                view_bookmark(logged, z)
            else:
                print(f'Seleccion incorrecta!!!')
elif x==2:
    create_user()
else:
    sys.exit()
