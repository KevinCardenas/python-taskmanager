import os, sys, getpass, json
from cryptography.fernet import Fernet

os.system('cls')

def iniciar_sesion():
    usuarios = open('usuarios.json', 'r')
    lista = json.load(usuarios)
    usuarios.close()
    email = input('Digite su correo :\n')
    password = getpass.getpass()
    i = 0
    while i < len(lista):
        l = False
        if email == lista[i]['email']:
            token = lista[i]['token']
            contra = lista[i]['password']
            token2 = bytes(token[2:-1], 'utf-8')
            contra2 = bytes(contra[2:-1], 'utf-8')
            f = Fernet(token2)
            des = f.decrypt(contra2)
            des2=str(des)[2:-1]
            if password == des2:
                i2=i
                print('SESION INICIADA')
                i=len(lista)*3
                l = lista[i2]
            else:
                print('Contraseña incorrecta!!!')
        else:
            print('Correo incorrecto!!!')
        i = i+1
    return l
