import os, sys, json, getpass
from cryptography.fernet import Fernet

os.system('cls')

def create_bookmark(login):
    llave = Fernet.generate_key()
    f = Fernet(llave)
    z=login
    book = open('bookmarks.json', 'r')
    lista = json.load(book)
    book.close()
    name = input('Digite el nombre del marcador : ')
    url = input('Digite la URL del marcador : ')
    valida=False
    while valida==False:
        if url=="":
            print("KEvin aprende a escribir pa")
            url = input('Digite la URL del marcador : ')
        else:
            print(print)
            if url.find('.')>=0 :
                print("URL Valido")
                valida=True
            else:
                url = input('Digite la URL del marcador : ')
                valida=False
    if url.find("www.") <0 :
        url="www."+url
        
    if url.find("http://") <0 :
        url="http://"+url
        print(url)
    a= input('Digite el usuario del sitio web : ')
    password = getpass.getpass()
    binn = password.encode('ascii')
    b = f.encrypt(binn)

    comentario = input('Digite su comentario: ')
    archivo2=open('labels.json','r')
    listalabel=json.load(archivo2)
    archivo2.close()
    numuser=0
    i=0
    while i<len(listalabel):
        if listalabel[i]['user']==z['email']:
            numuser=i
            j=0
            while j< len(listalabel[i]['label']):
                x=listalabel[i]['label'][j]
                print(f'{j}. {x}')
                j=j+1
        i=i+1

    sel=int(input(f'Selecciona el label que deseas asignar: '))
    var=listalabel[numuser]['label'][sel]

    new = {'name': name,
           'url': url ,
           'comentario' : comentario ,
           'label' : var,
            'login' : a,
           'password' : str(b),
            'correo' : z['email']
           }
    
    lista.append(new)
    archivo = open('bookmarks.json', 'w')
    json.dump(lista, archivo)
    archivo.close()
    print(F'Bookmark creado correctamente!!!')

def modify_bookmark(login):
    z=login
    book = open('bookmarks.json', 'r')
    lista = json.load(book)
    book.close()
    numuser=0
    i=0
    while i<len(lista):
        if lista[i]['correo']==z['email']:
            ax=i 
            x=lista[i]['name']
            print(f'{i}. {x}')
        i=i+1

    sel=int(input(f'Selecciona el marcador que deseas modificar: '))

    new_name = input(f'Digita el nuevo nombre del marcador : ')
    new_url = input(f'Digita la nueva url del marcador  : ')

    valida=False
    while valida==False:
        if new_url=="":
            print("KEvin aprende a escribir pa")
            new_url = input('Digite la new_url del marcador : ')
        else:
            print(print)
            if new_url.find('.')>=0 :
                print("new_url Valido")
                valida=True
            else:
                new_url = input('Digite la new_url del marcador : ')
                valida=False
    if new_url.find("www.") <0 :
        new_url="www."+new_url
        
    if new_url.find("http://") <0 :
        new_url="http://"+new_url
        print(new_url)
    new_coment = input(f'Digita el nuevo comentario : ')
    new_login = input(f'Digita el nuevo usuario del sitio web : ')
    new_password = input(f'Digita el nuevo password del usuario : ')
    
    archivo2=open('labels.json','r')
    listalabel=json.load(archivo2)
    archivo2.close()
    
    numuser=0
    i=0
    while i<len(listalabel):
        if listalabel[i]['user']==z['email']:
            numuser=i
            j=0
            while j< len(listalabel[i]['label']):
                x=listalabel[i]['label'][j]
                print(f'{j}. {x}')
                j=j+1
        i=i+1

    sel=int(input(f'Selecciona el label que deseas asignar: '))
    var=listalabel[numuser]['label'][sel]

    lista[ax] = {
        "name": new_name,
        "url": new_url,
        "comentario": new_coment,
        "label": var,
        "correo": z['email'],
        'login' : new_login,
        'password' : new_password
    }

    archivo = open('bookmarks.json','w')
    json.dump(lista, archivo)
    archivo.close()
    print(f'Marcador modificado correctamente!!!')

def erase_bookmark(login):
    z=login
    book = open('bookmarks.json', 'r')
    lista = json.load(book)
    book.close()

    i=0
    while i<len(lista):
        if lista[i]['correo']==z['email']:
            x=lista[i]['name']
            print(f'{i}. {x}')
        i=i+1
    sel = int(input(f'Digita el # del marcador que deseas eliminar: '))
    really=input(F'Estas seguro?: (s/n)')
    if really=='s' or really=='S':
        del lista[sel]
        print(f'Marcador borrado correctamente!!')

    else:
        print(f'Operacion anulada!!')

    archivo = open('bookmarks.json','w')
    json.dump(lista, archivo)
    archivo.close()

def view_bookmark(login, id):
    z=login
    i=int(id)
    book = open('bookmarks.json', 'r')
    lista = json.load(book)
    book.close()
    if z['email']==lista[i]['correo']:
        a=lista[i]
        b=a['name']
        c=a['url']
        d=a['comentario']
        e=a['label']
        f=a['login']
        g=a['password']
        print(f'Nombre: {b}')
        print(f'URL: {c}')
        print(f'Comentario: {d}')
        print(f'Label: {e}')
        print(f'login: {f}')
        print(f'password: {g}')
    else:
        print(f'ERROR!!')
    
def view_allbookmarks(login):
    z=login
    book = open('bookmarks.json', 'r')
    lista = json.load(book)
    book.close()
    print(f'Estos son todos tus marcadores!!!: ')
    
    i=0
    while i<len(lista):
        if lista[i]['correo']==z['email']:
            a=lista[i]
            b=a['name']
            c=a['url']
            d=a['label']
            print(f'{i}.')
            print(f'Nombre: {b}')
            print(f'URL: {c}')
            print(f'Label: {d}')
        else:
            print(f'ERROR!!')
        i=i+1


def view_label_bookmarks(login):
    z=login

    archivo2=open('labels.json','r')
    listalabel=json.load(archivo2)
    archivo2.close()

    i=0
    while i<len(listalabel):
        if listalabel[i]['user']==z['email']:
            numuser=i
            j=0
            while j< len(listalabel[i]['label']):
                x=listalabel[i]['label'][j]
                print(f'{j}. {x}')
                j=j+1
        i=i+1

    sel=int(input(f'Selecciona el label que deseas seleccionar: '))
    var=listalabel[numuser]['label'][sel]

    print(f'Estos son todos los marcadores asociados a: {var}')

    book = open('bookmarks.json', 'r')
    lista = json.load(book)
    book.close()

    i=0
    entro=False
    while i<len(lista):
        if lista[i]['label']==var:
            a=lista[i]
            b=a['name']
            c=a['url']
            d=a['label']
            print(f'{i}.')
            print(f'Nombre: {b}')
            print(f'URL: {c}')
            print(f'Label: {d}')
            entro=True
        elif entro==False:
            print(f'')
        i=i+1

            