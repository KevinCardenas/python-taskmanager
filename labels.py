import os, sys, json

os.system('cls')

def create_label(user):
    labels = open('labels.json', 'r')
    lista = json.load(labels)
    labels.close()
    name=input('Digite el nombre de la categoria: ')
    i=0
    while i <len(lista):
        if lista[i]['user']==user['email']:
            lista[i]['label'].append(name)
        i=i+1
    archivo = open('labels.json','w')
    json.dump(lista, archivo)
    archivo.close()
    print(F'Label creado correctamente!!!')

def modify_label(user):
    z=user
    labels = open('labels.json', 'r')
    listalabel = json.load(labels)
    labels.close()

    i=0
    while i<len(listalabel):
        if listalabel[i]['user']==z['email']:
            numuser=i
            j=0
            while j< len(listalabel[i]['label']):
                x=listalabel[i]['label'][j]
                print(f'{j}. {x}')
                j=j+1
        i=i+1

    sel=int(input(f'Selecciona el label que deseas modificar: '))
    nombre_nuevo=input('\n Ingrese el nombre nuevo : ')
    listalabel[numuser]['label'][sel]=nombre_nuevo

    archivo = open('labels.json','w')
    json.dump(listalabel, archivo)
    archivo.close()
    print('\n Creado correctamente !!')

def delete_label(user):
    z=user
    labels = open('labels.json', 'r')
    listalabel = json.load(labels)
    labels.close()

    i=0
    while i<len(listalabel):
        if listalabel[i]['user']==z['email']:
            numuser=i
            j=0
            while j< len(listalabel[i]['label']):
                x=listalabel[i]['label'][j]
                print(f'{j}. {x}')
                j=j+1
        i=i+1

    sel=int(input(f'Selecciona el label que deseas eliminar: '))
    del listalabel[numuser]['label'][sel]

    archivo = open('labels.json','w')
    json.dump(listalabel, archivo)
    archivo.close()
